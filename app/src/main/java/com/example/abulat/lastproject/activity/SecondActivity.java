package com.example.abulat.lastproject.activity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.abulat.lastproject.R;
import com.example.abulat.lastproject.pojo.Gifts;

/**
 * Created by abulat on 7/28/16.
 */
public class SecondActivity extends Activity {
    TextView lego;
    TextView book;
    TextView orange;
    TextView flowers;
    TextView socks;
    TextView chocolate;
    Gifts gift;
    TextView age;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_main);
        init();
        getGifts();
    }

    public void init() {
        lego = (TextView) findViewById(R.id.lego);
        book = (TextView) findViewById(R.id.book);
        orange = (TextView) findViewById(R.id.orange);
        flowers = (TextView) findViewById(R.id.flowers);
        socks = (TextView) findViewById(R.id.socks);
        chocolate = (TextView) findViewById(R.id.chocolate);
        age = (TextView) findViewById(R.id.age);
    }

    public void getGifts() {
        Bundle i = getIntent().getExtras();
        gift = i.getParcelable(getString(R.string.Birthday));
        lego.setText(gift.getLego());
        book.setText(gift.getBook());
        orange.setText(gift.getOrange());
        flowers.setText(gift.getFlowers());
        chocolate.setText(gift.getChocolate());
        socks.setText(gift.getSocks());
        age.setText(String.valueOf(gift.getAge()));
    }
}
