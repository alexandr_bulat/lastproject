package com.example.abulat.lastproject.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by abulat on 7/28/16.
 */
public class Gifts implements Parcelable {
    private String chocolate;
    private String orange;
    private String socks;
    private String book;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    private int age;

    public String getFlowers() {
        return flowers;
    }

    public void setFlowers(String flowers) {
        this.flowers = flowers;
    }

    private String flowers;

    public String getBook() {
        return book;
    }

    public void setBook(String book) {
        this.book = book;
    }

    public String getLego() {
        return lego;
    }

    public void setLego(String lego) {
        this.lego = lego;
    }

    private String lego;

    public String getSocks() {
        return socks;
    }

    public void setSocks(String socks) {
        this.socks = socks;
    }

    public String getOrange() {

        return orange;
    }

    public void setOrange(String orange) {
        this.orange = orange;
    }

    public String getChocolate() {

        return chocolate;
    }

    public void setChocolate(String chocolate) {
        this.chocolate = chocolate;
    }


    public Gifts() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.chocolate);
        dest.writeString(this.orange);
        dest.writeString(this.socks);
        dest.writeString(this.book);
        dest.writeInt(this.age);
        dest.writeString(this.flowers);
        dest.writeString(this.lego);
    }

    protected Gifts(Parcel in) {
        this.chocolate = in.readString();
        this.orange = in.readString();
        this.socks = in.readString();
        this.book = in.readString();
        this.age = in.readInt();
        this.flowers = in.readString();
        this.lego = in.readString();
    }

    public static final Creator<Gifts> CREATOR = new Creator<Gifts>() {
        @Override
        public Gifts createFromParcel(Parcel source) {
            return new Gifts(source);
        }

        @Override
        public Gifts[] newArray(int size) {
            return new Gifts[size];
        }
    };
}

