package com.example.abulat.lastproject;

import android.content.Context;
import android.widget.DatePicker;
import android.widget.Toast;

import com.example.abulat.lastproject.pojo.Gifts;

/**
 * Created by abulat on 7/28/16.
 */
public class GiftsMethods {
    public static final int MARTH = 2;
    public static final int THE_8_THNUMBER = 8;
    public static final int JANUARY = 0;
    public static final int THE_1_THNUMBER = 1;
    public static final int FEBRUARY = 1;
    public static final int THE_23_THNUMBER = 23;
    DatePicker dateBirthday;
    Gifts gift;
    Context context;
    DatePicker dateCurrent;

    public GiftsMethods() {
    }

    public GiftsMethods(Context context, DatePicker dateBirthday, DatePicker dateCurrent, Gifts gift) {
        this.dateBirthday = dateBirthday;
        this.gift = gift;
        this.context = context;
        this.dateCurrent = dateCurrent;
    }

    public void giftsGirls() {
        if (isGirlsDay(dateCurrent.getMonth(), dateCurrent.getDayOfMonth())) {
            gift.setFlowers("Flowers");
            giftLego();
            getAgeChild();
            Toast.makeText(context, "Girls", Toast.LENGTH_SHORT).show();
        } else if (dateBirthday.getMonth() == dateCurrent.getMonth() && dateCurrent.getDayOfMonth() == dateBirthday.getDayOfMonth()) {
            gift.setLego("Lego");
            giftNewYear();
            getAgeChild();
            Toast.makeText(context, "Data equals", Toast.LENGTH_SHORT).show();
        } else if (isNewYear(dateCurrent.getMonth(), dateCurrent.getDayOfMonth())) {
            gift.setChocolate("Big Black Chocolate");
            gift.setOrange("Orange");
            getAgeChild();
            Toast.makeText(context, "New Year", Toast.LENGTH_SHORT).show();
        } else {
            gift.setBook("Book");
            getAgeChild();
            Toast.makeText(context, "Girl", Toast.LENGTH_SHORT).show();
        }
    }

    public void giftsBoys() {
        if (isBoysDay(dateCurrent.getMonth(), dateCurrent.getDayOfMonth())) {
            gift.setSocks("Socks");
            getAgeChild();
            giftLego();
            Toast.makeText(context, "Boys", Toast.LENGTH_SHORT).show();
        } else if (dateBirthday.getMonth() == dateCurrent.getMonth() && dateCurrent.getDayOfMonth() == dateBirthday.getDayOfMonth()) {
            gift.setLego("Lego");
            giftNewYear();
            getAgeChild();
            Toast.makeText(context, "Data equals", Toast.LENGTH_SHORT).show();
        } else if (isNewYear(dateCurrent.getMonth(), dateCurrent.getDayOfMonth())) {
            Toast.makeText(context, "New Year", Toast.LENGTH_SHORT).show();
            gift.setChocolate("Big Black Chocolate");
            gift.setOrange("Orange");
            getAgeChild();
        } else {
            Toast.makeText(context, "Boy", Toast.LENGTH_SHORT).show();
            gift.setBook("Book");
            getAgeChild();
        }
    }

    public int calculateBirthday(int datePicker, int datePicker1) {
        int age = datePicker1 - datePicker;
        return age;
    }

    public void getAgeChild() {
        gift.setAge(calculateBirthday(dateBirthday.getYear(), dateCurrent.getYear()));
    }

    public boolean isBoysDay(int month, int day) {
        if (month == FEBRUARY && day == THE_23_THNUMBER) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isGirlsDay(int month, int day) {
        if (month == MARTH && day == THE_8_THNUMBER) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isNewYear(int month, int day) {
        if (month == JANUARY && day == THE_1_THNUMBER) {
            return true;
        } else {
            return false;
        }
    }

    public void giftLego() {
        if (dateBirthday.getMonth() == dateCurrent.getMonth() && dateCurrent.getDayOfMonth() == dateBirthday.getDayOfMonth()) {
            gift.setLego("Lego");
        }
    }

    public void giftNewYear() {
        if (isNewYear(dateCurrent.getMonth(), dateCurrent.getDayOfMonth())) {
            Toast.makeText(context, "New Year", Toast.LENGTH_SHORT).show();
            gift.setChocolate("Big Black Chocolate");
            gift.setOrange("Orange");
        }
    }

    public boolean getLeapYear(int leap) {
        if ((leap % 4 == 0) && leap % 100 != 0) {
            return true;
        } else if (leap % 400 == 0) {
            return true;
        } else {
            return false;
        }

    }
}
