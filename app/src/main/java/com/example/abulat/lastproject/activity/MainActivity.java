package com.example.abulat.lastproject.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.abulat.lastproject.GiftsMethods;
import com.example.abulat.lastproject.R;
import com.example.abulat.lastproject.pojo.Gifts;

public class MainActivity extends AppCompatActivity {
    public static final int sevenYear = 7;
    public static final int twelfthYear = 12;
    DatePicker dateBirthday;
    DatePicker dateCurrent;
    Spinner spinner;
    Button receive;
    Gifts gift;
    GiftsMethods giftsMethods;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    public void init() {
        dateBirthday = (DatePicker) findViewById(R.id.dateBirthday);
        dateCurrent = (DatePicker) findViewById(R.id.dateCurrent);
        spinner = (Spinner) findViewById(R.id.spinner);
        receive = (Button) findViewById(R.id.button);
        spinner.setOnItemSelectedListener(new CustomOnItemSelectedListener());
        receive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getgiftsChildrens();
            }
        });
    }

    public void getgiftsChildrens() {
        gift = new Gifts();
        giftsMethods = new GiftsMethods(this, dateBirthday, dateCurrent, gift);
        if (giftsMethods.calculateBirthday(dateBirthday.getYear(), dateCurrent.getYear()) >= sevenYear && giftsMethods.calculateBirthday(dateBirthday.getYear(), dateCurrent.getYear()) <= twelfthYear && spinner.getSelectedItem().toString().equals("Boy")) {
            if (giftsMethods.getLeapYear(dateCurrent.getYear())) {
                intentSecondclass();
            } else {
                giftsMethods.giftsBoys();
                intentSecondclass();
            }
        } else if (giftsMethods.calculateBirthday(dateBirthday.getYear(), dateCurrent.getYear()) >= sevenYear && giftsMethods.calculateBirthday(dateBirthday.getYear(), dateCurrent.getYear()) <= twelfthYear && spinner.getSelectedItem().toString().equals("Girl")) {
            if (giftsMethods.getLeapYear(dateCurrent.getYear())) {
            } else {
                giftsMethods.giftsGirls();
                intentSecondclass();
            }
        }
    }

    public void intentSecondclass() {
        Intent intent = new Intent(MainActivity.this, SecondActivity.class);
        intent.putExtra(getString(R.string.Birthday), gift);
        startActivity(intent);
    }

    public class CustomOnItemSelectedListener implements AdapterView.OnItemSelectedListener, AdapterView.OnItemClickListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            Toast.makeText(parent.getContext(), parent.getItemAtPosition(pos).toString(), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {

        }

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        }
    }
}
