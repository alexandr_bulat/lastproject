package com.example.abulat.lastproject;

import android.content.Context;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.DatePicker;

import com.example.abulat.lastproject.pojo.Gifts;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.stubVoid;
import static org.mockito.Mockito.verify;

/**
 * Created by abulat on 7/28/16.
 */
@RunWith(MockitoJUnitRunner.class)
public class GiftsMethodsTest {
    Gifts gifts;
    public static final int JANUARY = 0;
    public static final int MARTH = 2;
    public static final int THE_8_THNUMBER = 8;
    public static final int THE_1_THNUMBER = 1;
    public static final int FEBRUARY = 1;
    public static final int THE_23_THNUMBER = 23;
    GiftsMethods giftsMethods;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void get_Gifts_Boys() {
        gifts = new Gifts();
        giftsMethods = new GiftsMethods();
        assertEquals("Fall", 5, giftsMethods.calculateBirthday(1, 6));
    }

    @Test
    public void get_Leap_year() {
        gifts = new Gifts();
        giftsMethods = new GiftsMethods();
        assertTrue("FALSE", giftsMethods.getLeapYear(2016));
        assertFalse("TRUE", giftsMethods.getLeapYear(2015));
    }

    @Test
    public void get_is_newYear() {
        gifts = new Gifts();
        giftsMethods = new GiftsMethods();
        assertTrue("FALSE", giftsMethods.isNewYear(JANUARY, THE_1_THNUMBER));
        assertFalse("TRUE", giftsMethods.isNewYear(FEBRUARY, THE_23_THNUMBER));
    }

    @Test
    public void get_is_GirlsDay() {
        gifts = new Gifts();
        giftsMethods = new GiftsMethods();
        assertTrue("FALSE", giftsMethods.isGirlsDay(MARTH, THE_8_THNUMBER));
        assertFalse("TRUE", giftsMethods.isNewYear(FEBRUARY, THE_23_THNUMBER));
    }

    @Test
    public void get_is_BoysDay() {
        gifts = new Gifts();
        giftsMethods = new GiftsMethods();
        assertTrue("FALSE", giftsMethods.isBoysDay(FEBRUARY, THE_23_THNUMBER));
        assertFalse("TRUE", giftsMethods.isNewYear(MARTH, THE_8_THNUMBER));
    }
}